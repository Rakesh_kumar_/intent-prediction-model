# intent-prediction-model



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/Rakesh_kumar_/intent-prediction-model.git
git branch -M main
git push -uf origin main
```

## To Setup the Flask Code

1. create the virtual Env and activate it (Python version used: 3.10)
2. Install the requirements from requirements.txt
3. Run the app by:
```
python app.py
```
4. To check accuracy and precision scores run main.py
```
python main.py
```
5. To retrain the model and vectorizer pkl file uncomment the line 28, 29 in main.py and then run main.py
Lines
```
joblib.dump(vectorizer, 'intent_vectorizer.pkl')
joblib.dump(model, 'intent_model.pkl')
```