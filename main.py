# Import the required libraries
import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score, precision_score
from sklearn.model_selection import train_test_split
import joblib

# Load the dataset
workbook = pd.ExcelFile('intent_data.xlsx')
sheets = workbook.sheet_names

df = pd.concat([pd.read_excel(workbook, sheet_name=s)
                .assign(intent=s) for s in sheets])
df['Keyword'] = df['Keyword'].astype(str)

# Extract features using TF-IDF vectorizer
vectorizer = TfidfVectorizer()
X = vectorizer.fit_transform(df['Keyword'])

# Split the dataset into training and testing sets
X_train, X_test, y_train, y_test = train_test_split(X, df['intent'], test_size=0.2, random_state=42)

# Train the machine learning model
model = RandomForestClassifier()
model.fit(X_train, y_train)

# joblib.dump(vectorizer, 'intent_vectorizer.pkl')
# joblib.dump(model, 'intent_model.pkl')

# Evaluate the model
y_pred = model.predict(X_test)
accuracy = accuracy_score(y_test, y_pred)
precision = precision_score(y_test, y_pred, average='weighted')
print("Accuracy Score:", accuracy, "| Precision Score:", precision)

# # Deploy the model
# new_keyword = input("Enter Keyword: ")
# intent = model.predict(vectorizer.transform([new_keyword]))
# intent=str(intent).strip("['']")
# print("Intent:", intent)
