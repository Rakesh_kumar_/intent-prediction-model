"""
App.py for main app files
"""
import joblib
from flask import Flask, request, jsonify

app = Flask(__name__)

@app.route('/', methods=['GET'])
def home():
    """
    Home troute to handle home page render
    """
    return jsonify({'msg': "Hello World!"})

@app.route('/predict', methods=['POST'])
def predict():
    vector = joblib.load('intent_vectorizer.pkl')
    classifier = joblib.load('intent_model.pkl')
    prediction = classifier.predict(vector.transform([request.json['query']]))
    intent=str(prediction).strip("['']")
    return jsonify({'prediction': intent})


if __name__ == "__main__":
    app.run(host="127.0.0.1", port=8000, debug=True)